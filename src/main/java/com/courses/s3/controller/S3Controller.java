package com.courses.s3.controller;

import com.courses.s3.model.FileModel;
import com.courses.s3.service.S3Service;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/s3")
@RequiredArgsConstructor
public class S3Controller {

    private static final String DISPOSITION = "attachment; filename=\"%s\"";

    private final S3Service service;

    @PostMapping(path = "/{key}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void put(@PathVariable String key, @RequestParam("file") MultipartFile file) {
        service.upload(key, file);
    }

    @GetMapping("/{key}")
    public ResponseEntity<Resource> download(@PathVariable String key) {
        FileModel model = service.download(key);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(model.getMediaType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, String.format(DISPOSITION, model.getFileName()))
                .body(model.getResource());
    }

    @DeleteMapping("/{key}")
    public void delete(@PathVariable String key) {
        service.delete(key);
    }

    @PutMapping("/{key}")
    public void update(@PathVariable String key, MultipartFile file) {
        service.update(key, file);
    }

}
