package com.courses.s3.exception;

public class BucketDoesNotExistException extends RuntimeException {

    public BucketDoesNotExistException(String bucket) {
        super(String.format("Bucket with name %s does not exist", bucket));
    }
}
