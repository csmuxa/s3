package com.courses.s3.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.core.io.Resource;

@Getter
@Setter
public class FileModel {

    private String fileName;
    private String mediaType;
    private Resource resource;

}
