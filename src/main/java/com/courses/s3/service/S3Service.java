package com.courses.s3.service;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.courses.s3.exception.BucketDoesNotExistException;
import com.courses.s3.model.FileModel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.tika.mime.MimeTypes;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class S3Service {

    private static final String BUCKET_NAME = "jaxelcoursebucket";
    private static final MimeTypes MIME_TYPES = MimeTypes.getDefaultMimeTypes();

    private final AmazonS3Client client;

    @SneakyThrows
    public void upload(String key,MultipartFile file) {
        bucketExists();

        putObject(file, key);
    }

    @SneakyThrows
    public FileModel download(String key) {
        bucketExists();

        S3Object s3object = client.getObject(BUCKET_NAME, key);
        S3ObjectInputStream inputStream = s3object.getObjectContent();

        return new FileModel()
                .setResource(new InputStreamResource(inputStream))
                .setMediaType(s3object.getObjectMetadata().getContentType())
                .setFileName(key + extension(s3object));
    }

    public void delete(String key) {
        bucketExists();
        client.deleteObject(BUCKET_NAME, key);
    }

    @SneakyThrows
    public void update(String key, MultipartFile file) {
        bucketExists();

        Optional.ofNullable(client.getObject(BUCKET_NAME, key))
                .orElseThrow(RuntimeException::new);

        putObject(file, key);
    }

    @SneakyThrows
    private void putObject(MultipartFile file, String key) {
        client.putObject(
                BUCKET_NAME,
                key,
                file.getInputStream(),
                metadata(file.getContentType(), file.getSize())
        );
    }

    private ObjectMetadata metadata(String contentType, long contentLength) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(contentType);
        objectMetadata.setContentLength(contentLength);
        return objectMetadata;
    }

    @SneakyThrows
    private String extension(S3Object s3Object) {
        return MIME_TYPES.forName(s3Object.getObjectMetadata()
                .getContentType()).getExtension();
    }

    private void bucketExists() {
        if (!client.doesBucketExist(BUCKET_NAME))
            throw new BucketDoesNotExistException(BUCKET_NAME);
    }

}


