package com.courses.s3;

import com.amazonaws.services.s3.AmazonS3Client;
import com.courses.s3.config.S3Config;
import com.courses.s3.model.FileModel;
import com.courses.s3.service.S3Service;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

@Testcontainers
@SpringBootTest(classes = S3Config.class)
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class S3Test {

    private static final String BUCKET_NAME = "jaxelcoursebucket";

    @Autowired
    private S3Service service;
    @Autowired
    private AmazonS3Client client;

    @Container
    public static LocalStackContainer localStack =
            new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.10.0"))
                    .withServices(S3)
                    .withEnv("DEFAULT_REGION", "eu-west-3");

    @BeforeAll
    @SneakyThrows
    public static void beforeAll() {
        localStack.execInContainer("awslocal", "s3", "mb", "s3://" + BUCKET_NAME);
    }

    @AfterEach
    public void afterEach() {
        client.listObjects(BUCKET_NAME).getObjectSummaries()
                .forEach(s3ObjectSummary -> client.deleteObject(BUCKET_NAME, s3ObjectSummary.getKey()));
    }

    @Test
    public void NonExistingKeyThrowsException() {
        assertThrows(RuntimeException.class, () -> service.download("non existing key"));
    }

    @Test
    @SneakyThrows
    public void uploadAndDownload() {
        String key = "key";
        String fileName = "file";
        String mediaType = "text/plain";
        service.upload(key,
                new MockMultipartFile(fileName, "originalFileName.txt", mediaType, new byte[8]));

        FileModel download = service.download(key);

        assertEquals(key + ".txt", download.getFileName());
        assertEquals(mediaType, download.getMediaType());
        assertEquals(8, download.getResource().contentLength());
    }

    @Test
    @SneakyThrows
    public void update() {
        String key = "key";
        String fileName = "file";
        String mediaType = "image/jpeg";
        service.upload(key,
                new MockMultipartFile(fileName, "originalFileName.txt", "text/plain", new byte[8]));
        service.update(key, new MockMultipartFile(fileName, "anotherOrig.txt", mediaType, new byte[16]));

        FileModel download = service.download(key);

        assertEquals(key + ".jpg", download.getFileName());
        assertEquals(mediaType, download.getMediaType());
        assertEquals(16, download.getResource().contentLength());
    }

    @Test
    public void delete() {
        String key = "key";

        service.upload(key, new MockMultipartFile("file", "originalFileName.txt", "text/plain", new byte[8]));
        assertEquals(1, client.listObjects(BUCKET_NAME).getObjectSummaries().size());

        service.delete(key);
        assertEquals(0, client.listObjects(BUCKET_NAME).getObjectSummaries().size());
    }

}
